#   Copyright 2006 - 2008, Servprise International, Inc.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import java, sys

from java.lang import Runnable, Runtime, Thread
from com.servprise.webreboot import WebRebootConnection, WebReboot

###############################################################
#################### Utility functions. #######################
###############################################################

def input_with_default(prompt, default):
    """Utility function to allow prompts to have default values."""
    
    input = raw_input("%s (%s): " % (prompt, default))

    if input.strip() == "":
        input = default
        
    return input

def webreboot_in_use():
    """Tells whether the WebReboot is currently being accessed.
    
    This isn't 100% fool proof, since it only checks the background
    threads.  But the foreground thread shouldn't execute two commands
    at the same time and so long as it checks the background threads,
    things should be okay."""
    
    return ServerNameLoader.updating or UserNameLoader.updating


###############################################################
##################### Utility classes. ########################
###############################################################

class Server(object):
    """Represents a server connected to the WebReboot."""
    
    PLAIN_REBOOTABLE = "is_plain_rebootable"
    POWER_REBOOTABLE = "is_power_rebootable"
    
    def __init__(self, id, webreboot):
        self.webreboot = webreboot
        
        self.id = id
        self.__name = None
        self.__is_plain_rebootable = None
        self.__is_power_rebootable = None
        
    def __get_name(self):
        if self.__name is None:
            self.__name = self.webreboot.getServerName(self.id)
            
        return self.__name

    def __set_name(self, value):
        self.__name = value
        
    def __get_plain_rebootable(self):
        if self.__is_plain_rebootable is None:
            self.__is_plain_rebootable = self.webreboot.isPlainRebootable(self.id)
        
        return self.__is_plain_rebootable
    
    def __set_plain_rebootable(self, value):
        self.__is_plain_rebootable = value
    
    def __get_power_rebootable(self):
        if self.__is_power_rebootable is None:
            self.__is_power_rebootable = self.webreboot.isPowerRebootable(self.id)
        
        return self.__is_power_rebootable
    
    def __set_power_rebootable(self, value):
        self.__is_power_rebootable = value
        
    name = property(fget=__get_name, fset=__set_name)
    is_plain_rebootable = property(fget=__get_plain_rebootable, fset=__set_plain_rebootable)
    is_power_rebootable = property(fget=__get_power_rebootable, fset=__set_power_rebootable)

class User(object):
    """Represents a user on the WebReboot."""
    
    def __init__(self, id, webreboot):
        self.webreboot = webreboot
        
        self.id = id
        self.__name = None
        self.__servers = None
    
    def __get_name(self):
        if self.__name is None:
            self.__name = self.webreboot.getUserName(self.id)
            
        return self.__name
    def __set_name(self, value):
        self.__name = value

    def __get_servers(self):
        if self.__servers is None:
            # Note that the list() call is necessary to convert from a java.util.List to 
            # a Python list.  This is necessary because the two types do not have the same
            # set of methods.
            self.__servers = list(self.webreboot.getServerIDs(self.id))

        return self.__servers

    name = property(fget=__get_name, fset=__set_name)
    servers = property(fget=__get_servers)    
        
class ShutdownHandler(Runnable):
    """Utility class to log user out of WebReboot in a thread.
    
    This is really set up for java.lang.Runtime.addShutdownHandler
    so that it has a Runnable that can log the user out of the WebReboot
    should the program exit unexpectedly (e.g., uncaught exception, terminate signal)."""
    
    def __init__(self, webreboot):
        self.webreboot = webreboot
                
    def run(self):
        self.webreboot.logout()

class ServerNameLoader(Runnable):
    """Utility class to fetch the list of server names in a separate thread.
    
    The idea is that this can execute without blocking the main UI."""    

    updating = False

    def __init__(self, main):
        self.main = main
        
    def run(self):
        # Busy wait while any other thread is updating the cache of servers.
        while webreboot_in_use():
            pass
        
        # First, signal to the outside world that the cache of servers is not to be used.
        self.main.servers = None
        ServerNameLoader.updating = True
        
        # Retrieve the list of servers that the currently logged on user has permissions to access.
        server_ids = self.main.webreboot.getServerIDs(self.main.webreboot.getUserID())
            
        servers = []
            
        for server_id in server_ids:    
            server = Server(server_id, self.main.webreboot)
                
            server.name = self.main.webreboot.getServerName(server_id)
                
            servers.append(server)

        # Assign the local list to the class instance.  This signifies to anything outside this
        # thread that the class instance's server cache is now ready to be used.
        self.main.servers = servers
        ServerNameLoader.updating = False
        
class UserNameLoader(Runnable):
    """Utility class to fetch the list of user names in a separate thread.
    
    The idea is that this can execute without blocking the main UI."""    

    updating = False

    def __init__(self, main):
        self.main = main
        
    def run(self):
        # Busy wait while any other thread is updating the cache of servers.
        while webreboot_in_use():
            pass
        
        # First, signal to the outside world that the cache of users is not to be used.
        self.main.users = None
        UserNameLoader.updating = True
        
        # Retrieve the list of users from the WebReboot.
        user_ids = self.main.webreboot.getUserIDs()

        users = []
        
        for user_id in user_ids:
            user = User(user_id, self.main.webreboot)
            
            user.name = self.main.webreboot.getUserName(user_id)
            
            users.append(user)
        
        # Assign the local list to the class instance.  This signifies to anything outside this
        # thread that the class instance's user cache is now ready to be used.
        self.main.users = users
        UserNameLoader.updating = False
        
        
###############################################################
###################### Main program. ##########################
###############################################################
        
class Main(java.lang.Object):
    """Main application class."""
    
    def __init__(self):
        # Primary gateway for interacting with the WebReboot.
        self.webreboot = None
        
        # Cache of server attributes, used to cut down on costly calls to the WebReboot. 
        self.servers = None
        
        # Cache of user attributes, used to cut down on costly calls ot the WebReboot.
        self.users = None
        
    def get_menu_choice(self):
        ret = raw_input("\nEnter choice or 'Q' to go back: ")
        
        # If the user wants to go back, show the main menu again.
        # Note that this has a potential stack overflow issue if the user
        # session is to last a long time, with the user backtracking a lot.
        # The issue is related to non-returning calls.
        if ret.upper() == "Q":
            self.show_main_menu()
        else:
            return ret
    
    def login_prompt(self):
        """Prompts a user for connection information for a WebReboot.
        
        Returns a WebReboot instance."""
        
        print "Welcome to the WebReboot API test application.\n\n"
        
        print "Please enter the connection information for the server you would like to connect to.\n"
        scheme = input_with_default("Scheme", "http")
        hostname = input_with_default("Hostname", "192.168.1.225")
        port = input_with_default("Port", "80")
        
        print "\nNow please enter your login information.\n"
        username = input_with_default("User name", "admin")
        password = input_with_default("Password", "password")
                
        # Establish a connection to the WebReboot.
        self.webreboot = WebReboot()
        self.webreboot.connect(hostname, port, scheme)
        self.webreboot.login(username, password)
        
        return self.webreboot
    
    def change_password_prompt(self, user):
        new_password = raw_input("\nPlease enter the new password: ")
        
        self.webreboot.changePassword(user.id, new_password)
        
    def reload_server_names(self):
        """Update the cached list of servers."""
        
        Thread(ServerNameLoader(self)).start()
        
    def reload_user_names(self):
        """Update the cached list of users."""
        
        if self.webreboot.isAdmin():
            Thread(UserNameLoader(self)).start()
        
    def server_select_prompt(self, attribute=None):
        """Construct the menu of server names.  Only show names for which the predicate returns true."""
        
        # Busy loop until a background thread updates the server cache.
        while webreboot_in_use():
            pass
            
        # Build up the server selection menu based upon the supplied predicate and the list of
        # servers that the currently logged on user has permissions to access.
        for (index, server) in enumerate(self.servers):
            if attribute is None or getattr(server, attribute):
                print "%s) %s" % (str(index + 1), server.name)

        # Prompt the user for a server to reboot.
        choice = self.get_menu_choice()
    
        try:
            # Verify the server number is valid, prompting user to try again if it is not.
            choice = int(choice)
            if choice < 1 or choice > len(self.servers):
                print "\n\nInvalid selection.  Please try again.\n\n"
            
                return self.server_select_prompt(attribute)
        
            # Account for zero- vs one-indexing.
            ret = self.servers[choice - 1]
        
            # Now check that the server corresponding to the number is actually True for the attribute.
            # This may not be the case, e.g., if server #4 is not in the list, but the user types "4" anyway.
            # The user's choice would then be in the valid range and "ret" would could contain a server ID,
            # but the ID would not be valid for the action that the attribute is checking for.        
            if attribute is None or getattr(ret, attribute):
                return ret
            else:
                print "\n\nInvalid selection.  Please try again.\n\n"
            
                return self.server_select_prompt(attribute)
        
        # If the user didn't type a number, then the input was invalid.
        except ValueError:
            print "\n\nInvalid selection.  Please try again.\n\n"
            
            return self.server_select_prompt(attribute)
        
    def user_select_prompt(self):
        # Busy loop until a background thread updates the server cache.
        while webreboot_in_use():
            pass
        
        # Construct the menu of user names.
        for (index, user) in enumerate(self.users):
            print "%s) %s" % (str(index + 1), user.name)
        
        # Prompt the user for a user to work with.
        choice = self.get_menu_choice()
    
        try:
            
            # Verify the server number is valid.
            choice = int(choice)
            if choice < 1 or choice > len(self.users):
                print "\n\nInvalid selection.  Please try again.\n\n"
                
                return self.user_select_prompt()
        
            # Account for zero- versus one-indexing.
            return self.users[choice - 1]
        
        # If the user didn't type a number, then it's an invalid selection.
        except ValueError:
            print "\n\nInvalid selection.  Please try again.\n\n"
            
            return self.user_select_prompt()

    def show_plain_reboot_servers_menu(self):
        print "\nPlease choose a server to plain reboot.\n"
        
        # Get the server to plain reboot.
        server = self.server_select_prompt(Server.PLAIN_REBOOTABLE)
        self.webreboot.plainReboot(server.id)
    
    def show_power_reboot_servers_menu(self):
        print "\nPlease choose a server to power reboot.\n"
        
        # Get the server to power reboot.
        server = self.server_select_prompt(Server.POWER_REBOOTABLE)
        self.webreboot.powerReboot(server.id)
        
    def show_power_off_servers_menu(self):
        print "\nPlease choose a server to power off.\n"
        
        # Get the server to power off.
        server = self.server_select_prompt(Server.POWER_REBOOTABLE)
        self.webreboot.powerOff(server.id)
    
    def show_power_on_servers_menu(self):
        print "\nPlease choose a server to power on.\n"
        
        # Get the server to power on.
        server = self.server_select_prompt(Server.POWER_REBOOTABLE)
        self.webreboot.powerOn(server.id)
    
    def server_reboot_types_select_prompt(self, server):
        
        # Keep prompting the user for reboot types until the user ends the selection.
        while True:
            # Print out the plain rebootable info.
            if server.is_plain_rebootable:
                print "[+]",
            else:
                print "[-]",
            
            print " 1) Plain rebootable"
            
            
            # Print out the power rebootable info.
            if server.is_power_rebootable:
                print "[+]",
            else:
                print "[-]",
            
            print " 2) Power rebootable"
            
            
            # Now prompt the user to select the reboot types.
            print "\n'+' means that the server is rebootable via the specified method."
            print "'-' means it is not."
            
            choice = raw_input("\nEnter a reboot type number to change its state or 'Q' to stop: ")
            
            # Now that the user has given us his choice, see if we can toggle the reboot type.
            if choice == "1":
                server.is_plain_rebootable = not server.is_plain_rebootable
                
            elif choice == "2":
                server.is_power_rebootable = not server.is_power_rebootable
                
            # If the user did not pick one of the numbers or type 'Q' to exit, then it was invalid input.
            elif choice.upper() != "Q":
                print "\nInvalid selection.  Please try again."
                
            else:
                break
        
        # Once the user is done choosing, return the appropriate reboot type.
        if server.is_plain_rebootable and server.is_power_rebootable:            
            return self.webreboot.setPlainAndPowerRebootable
        
        elif server.is_plain_rebootable:
            return self.webreboot.setPlainRebootable
        
        elif server.is_power_rebootable:
            return self.webreboot.setPowerRebootable
        
        else:
            print "\n\n\nInvalid selection.  Must have at least a reboot type.  Try again.\n\n\n"
            
            server.is_plain_rebootable = True
            return self.server_reboot_types_select_prompt(server)
    
    def show_server_admin_menu(self):
        print "\nAvailable actions:\n"
        
        print "1) Rename servers"
        print "2) Change reboot type"
        
        choice = self.get_menu_choice()
            
        if choice == "1":
            print "\nPlease choose a server to rename.\n"
            server = self.server_select_prompt()
            
            new_name = raw_input("Enter a new name: ")
                
            # Reboot the server, accounting for the difference in indexing.
            self.webreboot.renameServer(server.id, new_name)
            
            # Update the cache of server names.
            server.name = new_name
            
        elif choice == "2":
            print "\nPlease choose a server to change reboot type for.\n"
            
            server = self.server_select_prompt()
            
            # Figure out which reboot type to set and then go ahead and set it.
            set_reboot_type = self.server_reboot_types_select_prompt(server)
            set_reboot_type(server.id)
        
        # Handle any invalid selection.
        else:
            self.show_server_admin_menu()
            
            
    def user_perms_select_prompt(self, server_ids, user_servers):
        """Builds up a list of servers a user can access in-place in user_servers.
        server_ids is the full set of servers from which user_servers can be derived."""
        
        # Busy loop until a background thread updates the server cache.
        while webreboot_in_use():
            pass
        
        # Separate from other text by printing a new line.
        print ""
        
        while True:
            # Build up the list of available servers, using +/- to indicate user permissions.
            for (index, server_id) in enumerate(server_ids):
                if server_id in user_servers:
                    print "[+]",
                else:
                    print "[-]",
                
                for server in self.servers:
                    if server.id == server_id:
                        print "%s) %s" % (str(index + 1), server.name)  
            
            print "\n'+' means the user has permissions to access the server."
            print "'-' means no permissions."
            
            choice = raw_input("\nEnter a server number to change its state or 'Q' to stop: ")
           
            # Now that the user has given us their choice, see if we can toggle the server.
            try:
                new_id = server_ids[int(choice) - 1]
                    
                if new_id in user_servers:
                    user_servers.remove(new_id)
                else:
                    user_servers.append(new_id)
            
            # If the user doesn't type a number, we're done.
            except ValueError:
                break
            
    def show_user_admin_menu(self):
        # Busy wait until the background thread updates the cache of users.
        while webreboot_in_use():
            pass
        
        # Used to determine if certain menu options are valid.
        users_empty = False
            
        if len(self.users) == 0:
            users_empty = True    
        
        print "\nAvailable actions:\n"
        
        print "1) Add user"
        
        if not users_empty:
            print "2) Remove user"
            print "3) Modify user permissions"
            print "4) Rename user"
            print "5) Change user password"
        
        choice = self.get_menu_choice()
                    
        if choice == "1":
            user_name = raw_input("New user's username: ")
            password = raw_input("New user's password: ")
            
            # Build up the parameters necessary for the user_perms_select_prompt() call.
            user_servers = []
            server_ids = self.webreboot.getServerIDs(self.webreboot.getUserID())
            
            # Update user_servers with the list of servers the user should have permissions to access.
            self.user_perms_select_prompt(server_ids, user_servers)
            
            # Now that everything is in order, actually add the user.
            self.webreboot.addUser(user_name, password, user_servers)
            
            print "\n\nAdded user.\n\n"
            
            # The user cache is now invalidated, and since we don't know the new user's ID,
            # the only thing we can do is discard the cache and start over.
            self.reload_user_names()
            
        elif not users_empty:    
            if choice == "2":
                print "\nPlease choose a user to remove.\n"
        
                user = self.user_select_prompt()
            
                confirm = input_with_default("Are you sure you want to delete this user? (y/n)", "n")
            
                if confirm == "y":
                    # Remove the user.
                    self.webreboot.removeUser(user.id)
                
                    # Update the cache of users.
                    self.users.remove(user)
                
                    print "\n%s was successfully removed from the WebReboot.\n" % user.name     
        
            elif choice == "3":
                print "\nPlease choose a user to change permissions for.\n"
            
                user = self.user_select_prompt()
            
                # Build up the parameters necessary for the user_perms_select_prompt() call.
                server_ids = self.webreboot.getServerIDs(self.webreboot.getUserID())
            
                # Update user_servers with the list of servers the user should have permissions to access.
                self.user_perms_select_prompt(server_ids, user.servers)
            
                # Now that everything is in order, go ahead and update the users permissions.
                self.webreboot.changePermissions(user.id, user.servers)
        
            elif choice == "4":
                print "\nPlease choose a user to rename.\n"
        
                user = self.user_select_prompt()
            
                new_username = raw_input("Enter the new username: ")
                new_password = raw_input("Enter the new password: ")
            
                self.webreboot.renameUser(user.id, new_username, new_password)
            
                # Update the user cache.
                user.name = new_username
            
            elif choice == "5":
                print "\nPlease choose a user to set the password for.\n"
        
                user = self.user_select_prompt()
            
                self.change_password_prompt(user.id)
                
            # Handle any invalid selection. 
            else:
                self.show_server_admin_menu()
            
        # Handle any invalid selection. 
        else:
            self.show_server_admin_menu()
    
    def show_main_menu(self):
        is_admin = self.webreboot.isAdmin()
        
        print "\nAvailable actions:\n"
        
        print "1) Change password"
        print "2) Plain reboot servers"
        print "3) Power reboot servers"
        print "4) Power off servers"
        print "5) Power on servers"
        
        # Print the admin menu if appropriate.
        if is_admin:
            print "======================"
            print "6) Server administration"
            print "7) User administration"
            print "8) Rename administrator account"
            print "9) Change WebReboot IP address"
        
        print "======================"
        print "Q) Quit"
        
        choice = raw_input("\nEnter choice: ")
          
        # First, handle choices available to all users.
        if choice == "1":
            self.change_password_prompt(self.webreboot.getUserID())
            
        elif choice == "2":
            self.show_plain_reboot_servers_menu()
            
        elif choice == "3":
            self.show_power_reboot_servers_menu()
        
        elif choice == "4":
            self.show_power_off_servers_menu()
        
        elif choice == "5":
            self.show_power_on_servers_menu()
            
        elif choice.upper() == "Q":
            self.webreboot.logout()
            
            sys.exit(0)
        
        # Now handle admin menu choices.
        elif is_admin:
            if choice == "6":
                self.show_server_admin_menu()
        
            elif choice == "7":
                self.show_user_admin_menu()
        
            elif choice == "8":
                new_username = raw_input("Enter the new username: ")
                new_password = raw_input("Enter the new password: ")
            
                self.webreboot.renameUser(self.webreboot.getUserID(), new_username, new_password)
        
            elif choice == "9":
                print "\nAfter changing the WebReboot IP, you will be logged out and will have to connect to the new IP.\n"
            
                new_ip = input_with_default("New IPv4 address", "192.168.1.225")
            
                # Actually change the IP
                self.webreboot.changeIP(new_ip)
            
                # Return True to indicate that the IP has changed.
                return True
            
            # Handle any invalid selection. 
            else:
                self.show_main_menu()
       
        # Handle any invalid selection. 
        else:
            self.show_main_menu()
        
        self.show_main_menu()
        
    def run_main(self):              
        # Log onto the WebReboot.
        self.webreboot = self.login_prompt()
    
        # If we couldn't actually log onto the WebReboot, prompt the user to try again. 
        while not self.webreboot.isLoggedIn():
            if self.webreboot.isInUse():
                print "\nSomeone else is currently logged into the WebReboot.  Please wait until it is available before trying again.\n"
                
                sys.exit()
                
            else:
                print "\nCould not connect with information provided.  Please try again.\n"
            
                # Allow the user to try again.
                self.webreboot = self.login_prompt()
        
        # Now that we're logged in, make sure that if we shutdown for any reason that we log off the WebReboot.
        Runtime.getRuntime().addShutdownHook(Thread(ShutdownHandler(self.webreboot)))
        
        # Start loading up the list of server names.
        self.reload_server_names()
        
        # Start loading up the list of user names.
        self.reload_user_names()
        
        # Show the appropriate menu of commands depending on user status.
        changed_IP = self.show_main_menu()   
        
        # If the user changed the WebReboot IP, start the main app over.
        if changed_IP:
            self.run_main()
            
        # Make sure we log out of the WebReboot when we exit.
        self.webreboot.logout()
            
# Handle running the program.
if __name__ == "__main__":
    try:
        main = Main()
        main.run_main()
    
    # If the user presses CTRL-C, just eat up the error message since
    # it's a valid exit condition.
    except EOFError:
        pass