GETTING STARTED
===============

This bundle contains an Eclipse project file to get you going right away.  The
project file, however, relies on both the Maven 2 plugin (http://m2eclipse.codehaus.org/)
and the PyDev plugin (http://pydev.sourceforge.net/).  Without them, you'll likely
receive a large number of error messages about missing dependencies.

This bundle also has everything necessary to build the WebReboot(R) Command Line Tool
yourself with maven.  This implies that you do have Maven 2.0.x installed from http://maven.apache.org/.
Before attempting to build the application, please open the pom.xml file and modify the value
for <jythonHome> by pointing it at your local Jython installation.


BUILDING THE APPLICATION
========================

Once you've modified the pom.xml file as described in the "GETTING STARTED" section,
you can build the application like so:

$ mvn package

where "$" is your system's shell prompt.


CODE LAYOUT
===========

Everything of interest is in a single Jython file stored under:

src/main/jython/com/servprise/webreboot/cli/Main.py

This Jython file illustrates how you can use the WebReboot API for Java without all the
overhead normally associated with a full-scale Java application.  Although Jython does not
share Java syntax, the code should be straightforward enough for a seasoned developer to follow.
Method calls are nearly identical in both languages and when using the WebReboot API, those calls
are what really matter.

Nonetheless, we acknowledge that we may be wrong and if this application is difficult to
follow along with, please send email to support@servprise.com indicating so.
and we will 